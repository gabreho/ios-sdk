//
//  LoginViewController.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

import SkrumbleSDK
import MBProgressHUD

class LoginViewController: UITableViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var me: User?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let config = ApiConfig(name: "sandbox",
                               apiHostname: "<api-url>",
                               authHostname: "<auth-url>",
                               clientID: "<client-id>",
                               clientSecrect: "<client-secret>")
        
        SKApi.apiConfig = config
    }
    
    func login() {
        
        let email = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        if email.isEmpty || password.isEmpty {
            
            let alert = UIAlertController(title: "Alert!", message: "Please enter both e-mail and pasword.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Logging in"
        
        SKApi.Auth.login(username: email, password: password).request { [unowned self, weak hud] (response) in
            hud?.hide(animated: true)
            if response?.success ?? false {
                SKApi.connectSocket()
                self.collectUserDataAndGoToMainViewController()
            }
        }
    }
    
    func collectUserDataAndGoToMainViewController() {
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Fetching user info"
        
        self.getUserInfo { [weak hud] in
            hud?.hide(animated: true)
            // TODO check if the request(s) was successful
            
            // go to Teams view controller
            
            self.performSegue(withIdentifier: "CurrentUserInfo", sender: self)
            
        }
        
    }
    
    func getUserInfo(_ completion: @escaping () -> Void) {
        var waiting = 1
        let checkIfDone = {
            waiting = waiting - 1
            if waiting <= 0 {
                MBProgressHUD.hide(for: self.view, animated: true)
                completion()
            }
        }
        
        SKApi.Users.me().request { [unowned self] (response) in
            if let response = response, response.success, let me = response.value {
                self.me = me
                checkIfDone()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let meViewController = segue.destination as? MeViewController, let me = me {
            meViewController.me = me
        }
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            login()
        }
    }
}
