//
//  Chat+Utils.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández on 7/8/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import SkrumbleSDK

extension Chat {
    
    
    /// For private chats: private chats have both users stored, so this this
    /// methos is for getting the other participant user.
    /// - Parameter me: current user
    /// - Returns: the other user in the private chat
    func getOtherUser(_ me: User) -> User? {
        
        if self.type == Chat.ChatType.room {
            return nil
        }
        
        return self.users?.filter({ (u) -> Bool in
            return u.id != me.id
        }).first
    }
    
    func getChatName(_ me: User) -> String {
        
        if self.type == .room {
            return self.name
        }
        
        return getOtherUser(me)?.fullName ?? ""
        
    }
}
