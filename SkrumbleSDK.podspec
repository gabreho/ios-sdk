#
# Be sure to run `pod lib lint SkrumbleSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SkrumbleSDK'
  s.version          = '0.1.3'
  s.summary          = 'SDK for 3rd party developers wanting to use the Skrumble API in their iOS Application.'

  s.description      = <<-DESC
The official Skrumble iOS SDK is a convenient, Swift-based toolkit for working with the Skrumble API. Works over HTTP or websocket, allowing real-time chat applications built on top of Skrumble.
                       DESC

  s.homepage         = 'https://gitlab.com/skrumble/ios-sdk'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Skrumble' => 'dev.support@skrumble.com' }
  s.source           = { :git => 'https://gitlab.com/skrumble/ios-sdk.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/SkrumbleHQ'

  s.platform = :ios
  s.ios.deployment_target = '10.0'
  s.swift_version = '4.1'

  s.source_files = 'SkrumbleSDK/Classes/**/*'

  s.dependency 'Alamofire', '~> 4.7.3'
  s.dependency 'SwiftyJSON', '~> 4.2.0'
  s.dependency 'Socket.IO-Client-Swift', '~> 12.0.0'
end
