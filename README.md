# SkrumbleSDK

<!--[![CI Status](https://img.shields.io/travis/gabreho/SkrumbleSDK.svg?style=flat)](https://travis-ci.org/gabreho/SkrumbleSDK)-->
<!--[![Version](https://img.shields.io/cocoapods/v/SkrumbleSDK.svg?style=flat)](https://cocoapods.org/pods/SkrumbleSDK)-->
<!--[![License](https://img.shields.io/cocoapods/l/SkrumbleSDK.svg?style=flat)](https://cocoapods.org/pods/SkrumbleSDK)-->
<!--[![Platform](https://img.shields.io/cocoapods/p/SkrumbleSDK.svg?style=flat)](https://cocoapods.org/pods/SkrumbleSDK)-->


## Usage
The SkrumbleSDK is available through [CocoaPods](https://cocoapods.org).


1. In your Podfile, add the following line:  
   ```swift
   pod 'SkrumbleSDK', :git => 'https://gitlab.com/skrumble/ios-sdk'
   ```
2. On the command-line, install your pods:
   ```bash
   pod install --repo-update
   ```


### Configuration
```swift
import SkrumbleSDK

let config = ApiConfig(name: "sandbox",
                       apiHostname: "<api-url>",
                       authHostname: "<auth-url>",
                       clientID: "<client-id>",
                       clientSecrect: "<client-secret>")

SKApi.apiConfig = config
```

#### Login
After a successful login, `SKApi.accessToken` will be assigned the session's access token. To persist the session, save this object and set it as needed. With an access token set, you can connect to the Skrumble API over a websocket to receive real-time socket events, like messages or new chats.

```swift
import SkrumbleSDK

SKApi.Auth.login(username: "user@mail.com", password: "password").request { [weak self] (response) in
    if response?.success ?? false {
        SKApi.connectSocket()
        // do something else
        self?.doSomethingAfterLogin()
    }
}
```

#### Get user info
```swift
import SkrumbleSDK

SKApi.Users.me().request { (response) in
    if let me = response?.value {
        //.. do something else
    }
}
```

#### Get Chats
```swift
import SkrumbleSDK

SKApi.Chats.getChats(teamId: ":id").request { (response) in
    if let chats = response?.arrayValue {
        // ...
    }
}
```

## Example project
To run the example project, clone the repo, and run `pod install` from the `example/` directory first.

## Troubleshooting CocoaPods 
Due to the large number of pods available, CocoaPods uses separate commands for installing pods or updating the list of which pods are available. Because developers must manually update this list, sometimes it can fall behind and give the error `Cannot find compatible versions for pod ...`

Because of this, we recommend installing pods with the `--repo-update` flag, which will update the list of available pods while installing. To update the list without installing, run:
```bash
pod update
```

If all else fails, it may be required to flush the pod caches before updating:
```bash
rm -fr ~/Library/Caches/CocoaPods && rm -fr Pods && pod update
```


## Author
Skrumble, dev.support@skrumble.com

## License
SkrumbleSDK is available under the MIT license. See the LICENSE file for more info.
