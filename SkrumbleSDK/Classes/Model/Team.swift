//
//  Team.swift
//  SDK_Demo
//
//  Created by 李莉 on 2018/4/27.
//  Copyright © 2018 InfChain. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

@objc open class Team: SKObject {
    
    public enum ActivatedState: NSInteger {
        case pending = 0
        case paywall = 1
        case activated = 2
    }
    
    public enum Role: NSInteger {
        case admin = 0
        case member = 1
        case guest = 2
    }

    
    // Display name of the team.
    public var name: String?
    
    //Team name with special characters removed. Used to create URL-friendly guest links.
    public var slug: String?
    public var role: Role?
    public var position: String?

    public var billingId: String?
    public var teamExtension:[String] = []
    
    public var firstName: String?
    public var lastName: String?
    public var color: String?
    
    
    public var users: [User]?
    
    // The User who created this team.
    public var owner: String?
    
    
    public var homeNumber: String?
    public var workNumber: String?
    
    //The URL of the team's avatar.
    
    public var teamAvatar: String?
    
    public var thumbUrl: String?

    
    //    Team's postal or zip code

    public var address:Address?
    
    public var activated:ActivatedState?
    
    // The TZ string of this team's timezone
    
    public var timeZone: String?
    
    // What point of setting-up autoreception is complete. json are `completed` @todo
    
    public var autoReception: String?
    
    //The default caller ID name field for all members.
    
    public var callerIdName: String?
    
    //The default caller ID number field for all members.
    
    public var callerIdNumber: String?
    
    public override init(with json: JSON) {
        super.init(with: json)
        if json["role"].stringValue.isEqual("admin") {
            self.role = Role.admin
        }else if json["role"].stringValue.isEqual("member") {
            self.role = Role.member
        }else if json["role"].stringValue.isEqual("guest") {
            self.role = Role.guest
        }
        self.owner = json["owner"].stringValue
        self.position = json["position"].stringValue
        self.billingId = json["billing_id"].stringValue
        
        self.workNumber = json["work_number"].string
        self.homeNumber = json["home_number"].string
        
        for ext in json["extension"].arrayValue {
            self.teamExtension.append(ext.stringValue)
        }
        
        self.firstName = json["first_name"].stringValue
        self.lastName = json["last_name"].stringValue
        self.color = json["color"].stringValue
        self.thumbUrl = json["thumbUrl"].stringValue
        
        self.name = json["team_name"].stringValue
        self.teamAvatar = json["team_avatar"].stringValue
        self.slug = json["slug"].stringValue
        self.address = Address.init(city: json["city"].stringValue, country: json["country"].stringValue, postal: json["postal"].stringValue, state: json["state"].stringValue, address1: json["address_1"].stringValue, address2: json["address_2"].stringValue)
        if (json["activated"].stringValue.isEqual("pending")) {
            self.activated = .pending
        }else if(json["activated"].stringValue.isEqual("paywall")) {
            self.activated = .paywall
        }else if(json["activated"].stringValue.isEqual("activated")) {
            self.activated = .activated
        }

        self.timeZone = json["timezone"].stringValue
        self.autoReception = json["autoreception"].stringValue
        self.callerIdName = json["caller_id_name"].stringValue
        self.callerIdNumber = json["caller_id_number"].stringValue
        
        for  obj in json["users"] {
            let usr = User.init(with: JSON(obj))
            self.users?.append(usr)
        }
    }
    
    public struct Address {
        var city: String?
        var country: String?
        var postal: String?
        var state: String?
        var address1: String?
        var address2: String?
        
        init(city:String, country:String, postal:String, state:String, address1:String, address2:String) {
            self.city = city
            self.country = country
            self.postal = postal
            self.state = state
            self.address1 = address1
            self.address2 = address2
        }
    }
}
