//
//  SkrumbleObject.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 5/22/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SwiftyJSON

func ==(lhs: SKObject, rhs: SKObject) -> Bool {
    return lhs.id == rhs.id
}

open class SKObject: NSObject {
    
    override open var hashValue: Int {
        return self.id.hashValue
    }
    
    override open func isEqual(_ object: Any?) -> Bool {
        if let other = object as? SKObject {
            return other == self
        }
        return false
    }
    
    public var id: String
    public var createdAt: Date?
    
    override init() {
        self.id = ""
        super.init()
    }
    
    init(with json: JSON) {
        self.id = json["id"].stringValue
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        
        self.createdAt = dateFormatter.date(from: json["created_at"].stringValue)
        
    }
    
    convenience init(with dict: [String: Any]) {
        let json = JSON(dict)
        self.init(with: json)
    }
}
