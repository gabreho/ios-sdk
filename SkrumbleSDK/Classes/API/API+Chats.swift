//
//  API+Chats.swift
//  Alamofire
//
//  Created by Uri Frischman on 2018-06-27.
//

import Foundation
import SwiftyJSON

extension SKApi {
    public enum Chats : APIEndpoint {
        case getChats(teamId: String?)
        case getSingleChat(chatId : String)
        case deleteChat(chatId : String)
        case createChat(name : String, purpose : String, type : Chat.ChatType, teamId : String, users : [String])
        case updateChat(chatId : String, newName : String, newPurpose : String, locked : Bool)
        
        public func fillResponse(_ response: inout APIResponse<Chat>) {
            switch self {
            case .getChats:
                guard let json = response.json else { break }
                var chats: [Chat] = []
                let arrJson = json.arrayValue
                for chatJson in arrJson {
                    let chat = Chat(with: chatJson)
                    chats.append(chat)
                }
                response.array = chats
                break
            case .getSingleChat, .createChat:
                guard let json = response.json else {break}
                let chat = Chat(with: json)
                response.value = chat
                break;
            case .updateChat:
                guard let json = response.json else {break}
                guard let chatJson = json.arrayValue.first else { break }
                let chat = Chat(with: chatJson)
                response.value = chat
                break
            default:
                break;
            }
        }
        
        public typealias APIResponseType = APIResponse<Chat>
        
        public func request(_ completion: ((APIResponse<Chat>?) -> Void)?) {
            switch self {
            default:
                _sendRequest(completion: completion)
                break
            }
        }
        
        public var endpoint: String {
            switch self {
            case .getChats, .createChat:
                return "/chat"
            case .getSingleChat(let chatId), .deleteChat(let chatId), .updateChat(let chatId,_,_,_):
                return "/chat/" + chatId
            }
        }
        
        public func parameters() -> [String: Any]? {
            var params: [String : Any] = [:]
            switch self {
            case .getChats(let teamId):
                params["populate"] = "users,last_message"
                params["unread"] = "true"
                params["limit"] = 1000
                params["skip"] = 0
                
                if let teamId = teamId {
                    params["team"] = teamId
                }
                break
            case .getSingleChat:
                params["populate"] = "messages, users, owner"
                break
            case .createChat(let name, let purpose, let type, let teamId, let users):
                params["name"] = name
                params["purpose"] = purpose
                params["team"] = teamId
                params["users"] = users
                if type == Chat.ChatType.private {
                    params["type"] = "private"
                } else if type == Chat.ChatType.room {
                    params["type"] = "room"
                }
                break
            case .updateChat(_, let newName, let newPurpose, _):
                // Is the variable 'locked' used to set any other params?
                params["name"] = newName
                params["purpose"] = newPurpose
                break
            case .deleteChat:
                break
            }
            return params
        }
        
        public func method() -> String {
            switch self {
            case .createChat:
                return "POST"
            case .getChats, .getSingleChat:
                return "GET"
            case .updateChat:
                return "PATCH"
            case .deleteChat:
                return "DELETE"
            }
        }
    }
}

