//
//  Skrumble.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 5/24/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation

//typealias SkrumbleAPI = API


public struct ApiConfig {

    
    var name: String
    var apiHostname: String
    var authHostname: String
    var clientID: String
    var clientSecrect: String
    
    public init(name: String, apiHostname: String, authHostname: String, clientID: String, clientSecrect: String) {
        self.name = name
        self.apiHostname = apiHostname
        self.authHostname = authHostname
        self.clientID = clientID
        self.clientSecrect = clientSecrect
    }
}

let DebugAPICalls = 3

class SocketListener: NSObject {
    static let shared = SocketListener()
    
    private override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(SocketListener.socketConnected), name: .socketRegistered, object: nil)

    }
    
    @objc public func socketConnected() {
        SKApi.flushAPICalls() // just by callin one will flush everything... TODO gabriel find a better way to do this
    }
}

public enum StatusCode: Int {
    case none = 0
    case ok = 200
    case created = 201
    case accepted = 202
    case noContent = 204
    case badRequest = 400
    case unauthorized = 401
    case notFound = 404
    case networkLost = -1005
    
    func success() -> Bool {
        let raw: Int = self.rawValue
        return raw >= StatusCode.ok.rawValue && raw < StatusCode.badRequest.rawValue
    }
    func failure() -> Bool {
        let raw: Int = self.rawValue
        return (raw >= StatusCode.badRequest.rawValue)
    }
}

public struct SKApi {
    
    static let PAGE_SIZE = 30
    
    static var refreshingToken: Bool = false
    
    static var keys: [UUID] = []
    static var queueAPIEndpoints: [AnyHashable : Any] = [:]
    static var queueAPICallbacks: [AnyHashable : Any?] = [:]
    
    public static var apiConfig: ApiConfig!
    
    public static var accessToken: AccessToken?
    
    public static func connectSocket() {
        SocketManager.connectSocket()
    }
    
    public static func disconnectSocket() {
        SocketManager.disconnect()
    }
    
    public static func addChatSocketObserverDelegate(_ delegate: ChatSocketObserverDelegate) {
        SocketManager.getSharedInstance().chatSocketObserver.registerListener(delegate)
    }
    
    public static func removeChatSocketObserverDelegate(_ delegate: ChatSocketObserverDelegate) {
        SocketManager.getSharedInstance().chatSocketObserver.unregisterListener(delegate)
    }
    
    static func queueApiCall<T: APIEndpoint>(_ apiEndpoint: T, callback: T.APICallback?) {
        let uuid = UUID()
        SKApi.queueAPIEndpoints[uuid] = apiEndpoint
        SKApi.queueAPICallbacks[uuid] = callback
        
        SKApi.keys.append(uuid)
    }
    
    static func flushAPICalls() {
        repeat {
            guard let uuid = SKApi.keys.last else { return }
            let callback = SKApi.queueAPICallbacks[uuid]
            
            
            
            SKApi.queueAPIEndpoints[uuid] = nil
            SKApi.queueAPICallbacks[uuid] = nil
            SKApi.keys.removeLast()
        } while SKApi.keys.count > 0
    }
    
    struct Pager {
        var limit: Int = PAGE_SIZE
        var skip: Int = 0
        private(set) var lastPage = false
        mutating func next() {
            if lastPage {
                return
            }
            if skip < 0 {
                skip = 0
            }
            skip += limit
        }
        
        mutating func previous() {
            if skip - limit < 0 {
                skip = 0
            } else {
                skip -= limit
            }
        }
        
        mutating func reset() {
            skip = 0
            lastPage = false
        }
    }
}

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    var queryString: String {
        var pairs: [String] = []
        for (k, v) in self {
            pairs.append("\(k)=\(v)")
        }
        if pairs.count > 0 {
            return "?" + pairs.joined(separator: "&")
        }
        return ""
    }
}
