//
//  API+Auth.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 5/24/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import SwiftyJSON

public extension SKApi {
    public enum Auth : APIEndpoint {
        
        public func fillResponse(_ response: inout APIResponse<AccessToken>) {
            switch self {
            case .login, .refreshToken:
                guard let json = response.json else { return }
                let token = AccessToken(with: json)
                response.value = token
                SKApi.accessToken = token
                
                break
            default:
                break
            }
        }
        
        public typealias APIResponseType = APIResponse<AccessToken>
        
        public var sendToSocket: Bool {
            switch self {
            case .sendPushToken, .unsubscribePushToken:
                return true
            default:
                return false
            }
        }
        
        
        public var endpoint: String {
            switch self {
            case .login:
                return "/login-user"
            case .refreshToken:
                return "/oauth/token"
            case .sendPushToken(let token), .unsubscribePushToken(let token):
                return "/user/subscribe/" + token
            default:
                break
            }
            return "/"
        }
        
        public func request(_ completion: ((APIResponse<AccessToken>?) -> Void)?) {
            _sendRequest(completion: completion)
        }
        
        public func parameters() -> [String : Any]? {
            var params: [String : String] = [:]
            switch self {
            case .login(let username, let password):
                params = [
                    "grant_type": "password",
                    "username": username,
                    "password": password,
                    "client_id": SKApi.apiConfig.clientID,
                    "client_secret": SKApi.apiConfig.clientSecrect
                ]
                break
            case .refreshToken(let refreshToken):
                params = [
                    "grant_type": "refresh_token",
                    "refresh_token": refreshToken,
                    "client_id": SKApi.apiConfig.clientID,
                    "client_secret": SKApi.apiConfig.clientSecrect
                ]
                break
            case .sendPushToken(let token):
                #if DEBUG
                let debug = "true"
                #else
                let debug = "false"
                #endif
                
                params = [
                    "device_type": "ios",
                    "registration_id": token,
                    "debug": debug
                ]
                
                break
            default:
                break
            }
            return params
        }
        
        public func method() -> String {
            switch self {
            case .login, .logout, .refreshToken, .sendPushToken:
                return "POST"
            case .unsubscribePushToken:
                return "DELETE"
            }
        }
        
        case unsubscribePushToken(token: String)
        case sendPushToken(token: String)
        case logout()
        case login(username: String, password: String)
        case refreshToken(token: String)
        
    }
}
