//
//  SocketEventProcessor.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/17/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO

@objc protocol SocketEventProcessor: class {
    
    // make sure to have a skrumble locally before processing any socket
    func createSocketObjetIfNotExistOnSocketEvent(dict: [String: Any])
    
    func onUpdated(socketEventObject: SocketEventObject)
    func onAddedTo(socketEventObject: SocketEventObject)
    func onRemovedFrom(socketEventObject: SocketEventObject)
}
