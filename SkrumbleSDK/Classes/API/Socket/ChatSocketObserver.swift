//
//  ChatSocketObserver.swift
//  SkrumbleSDK
//
//  Created by Gabriel Hernández on 7/7/18.
//

import Foundation
import SwiftyJSON

@objc public  protocol ChatSocketObserverDelegate: class {
    
    @objc func incomingMessage(_ message: Message, chatId: String)
}

class ChatSocketObserver: ObservableObject {
    
    override init(_ socket: Socket) {
        super.init(socket)
        self.socket.registerObserver(self)
    }
    
    // MARK: - Observer
    
    override func canHandleEvent(event: String) -> Bool {
        return event == "chat"
    }
    
    override func onAddedTo(socketEventObject: SocketEventObject) {
        let data = socketEventObject.response
        let id = socketEventObject.id
        
        guard let attributes = data["attribute"] as? String else { return }
        
        if attributes == "messages" {
            
            let json = JSON(data)["added"]
            let message = Message(with: json)
            
            
            
                for listener in listeners {
                    if let listener = listener.value as? ChatSocketObserverDelegate {
                        listener.incomingMessage(message, chatId: id)
                    }
                }
            
        }
        
        
    }
    
    override func onUpdated(socketEventObject: SocketEventObject) {
    }
    
    override func onRemovedFrom(socketEventObject: SocketEventObject) {
    }
    
    override func onUnhandled(verb: String, event: SocketAnyEvent) {
        
    }
    
}
