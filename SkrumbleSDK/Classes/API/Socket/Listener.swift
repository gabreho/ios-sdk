//
//  Listener.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/16/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation

@objc protocol Listener: class {
    func registerListener(_ listener: AnyObject)
    func unregisterListener(_ listener: AnyObject)
    func unregisterAllListeners()
}
