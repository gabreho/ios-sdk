//
//  ObservableObject.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/16/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO

class Weak<T: AnyObject> {
    weak var value : T?
    init (value: T) {
        self.value = value
    }
}

extension Array where Element:Weak<AnyObject> {
    mutating func reap () {
        self = self.filter { nil != $0.value }
    }
}

@objc class ObservableObject: NSObject, Observer, Listener, SocketEventProcessor {
    typealias ObserverCallback = ((_ status: Int, _ response: Any?) -> Void)?
    
    @objc var socket: Socket!
    
    // useless
    var listeners = [Weak<AnyObject>]()
    
    @objc init(_ socket: Socket) {
        super.init()
        self.socket = socket
    }
    
    // --------------------------------------
    // MARK: - Observer
    
    @objc func canHandleEvent(event: String) -> Bool {
        preconditionFailure("This method must be overridden.")
    }
    
    @objc func notify(_ event: SocketAnyEvent) -> Bool {
        if (self.canHandleEvent(event: event.event)) {
            processEvent(event)
            return true
        }
        return false
    }
    
    @objc func processEvent(_ event: SocketAnyEvent) {
        let socketEventObject = SocketEventObject(event: event)
        
        if (socketEventObject.response.count == 0) {
            return;
        }
        
        if (socketEventObject.verb.count == 0) {
            return;
        }
        
        switch socketEventObject.verb {
    // Update events
        case Socket.Verb.Updated.rawValue:
            createSocketObjetIfNotExistOnSocketEvent(dict: socketEventObject.previous)
            onUpdated(socketEventObject: socketEventObject)
            break
    // Add events
        case Socket.Verb.AddedTo.rawValue:
            createSocketObjetIfNotExistOnSocketEvent(dict: socketEventObject.added)
            onAddedTo(socketEventObject: socketEventObject)
            break
    // Remove events
        case Socket.Verb.RemovedFrom.rawValue:
            onRemovedFrom(socketEventObject: socketEventObject)
            break
        default:
            onUnhandled(verb: socketEventObject.verb, event: event)
        }
        
//        var attributesLog = ""
//        if (socketEventObject.attribute.count > 0) {
//            attributesLog = String(format: " - [atttributes = %@]", socketEventObject.attribute)
//        }
    }
    
    func mergeDict(origin: NSDictionary, new: NSDictionary) -> NSDictionary {
        let dict = origin.mutableCopy() as! NSMutableDictionary
        
        for (key, value) in new {
            dict.setValue(value, forKeyPath: key as! String)
//            dict[key] = value
        }
        
        return dict.copy() as! NSDictionary
    }
    
// --------------------------------------
// MARK: - Listener
    
    @objc func registerListener(_ listener: AnyObject) {
        Lock.synced(self) {
            listeners.append(Weak(value: listener))
        }
    }
    
    @objc func unregisterListener(_ listener: AnyObject) {
        Lock.synced(self) {
            var indexToRemove = -1;
            
            for (index, element) in listeners.enumerated() {
                if element === listener {
                    indexToRemove = index
                    break
                }
            }
            
            if indexToRemove >= 0 {
                listeners.remove(at: indexToRemove)
            }
        }
    }
    
    @objc func unregisterAllListeners() {
        Lock.synced(self) {
            listeners.removeAll()
        }
    }
    
// MARK: - SocketEventProcessor
    
    @objc func createSocketObjetIfNotExistOnSocketEvent(dict: [String : Any]) {
        // TO IMPLEMENT BY SUBCLASS
    }
    
    @objc func onUpdated(socketEventObject: SocketEventObject) {
        // TO IMPLEMENT BY SUBCLASS
    }
    
    @objc func onAddedTo(socketEventObject: SocketEventObject) {
        // TO IMPLEMENT BY SUBCLASS
    }
    
    @objc func onRemovedFrom(socketEventObject: SocketEventObject) {
        // TO IMPLEMENT BY SUBCLASS
    }
    
    @objc func onUnhandled(verb:String, event: SocketAnyEvent) {
        
    }
}
