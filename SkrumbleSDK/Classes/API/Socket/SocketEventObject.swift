//
//  SocketEventObject.swift
//  NuageTel
//
//  Created by Arnaud on 2017-08-28.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO

@objc class SocketEventObject: NSObject {
    
    let response: [String: Any]
    
    let id: String
    let verb: String
    let attribute: String
    let event: SocketAnyEvent!
    
    var data: [String: Any] {
        get { return self.response["data"] as! [String : Any] }
    }
    
    var previous: [String: Any] {
        get {
            return self.response["previous"] as? [String : Any] ?? [:]
        }
    }
    
    var added: [String: Any] {
        get { return self.response["added"] as! [String : Any] }
    }
    
    init(event: SocketAnyEvent) {
        self.event = event
        
        if let response = event.items?[0] as? [String: Any] {
            self.response = response
        } else {
            self.response = [:]
        }
        
        self.verb = self.response["verb"] as! String
        self.id = self.response["id"] as! String
        if let attr =  self.response["attribute"] as? String {
            self.attribute = attr
        } else {
            self.attribute = ""
        }
    }
}
