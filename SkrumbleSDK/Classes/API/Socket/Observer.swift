//
//  Observer.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/16/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO

@objc protocol Observer: class {
    func notify(_ event: SocketAnyEvent) -> Bool
}
