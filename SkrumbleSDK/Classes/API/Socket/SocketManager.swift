//
//  SocketManager.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/12/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON

@objc class SocketManager: NSObject {
    static private var sharedInstance: SocketManager? = nil
    
    @objc let chatSocketObserver: ChatSocketObserver!
    
    private let socket: Socket!
    
    @objc static func getSharedInstance() -> SocketManager! {
        if (sharedInstance == nil) {
            sharedInstance = SocketManager()
        }
        return sharedInstance
    }
    
    @objc deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    @objc override init() {
        
//        NULog.logInstanceInit("Socket Manager")
        socket = Socket()
        chatSocketObserver = ChatSocketObserver(socket)
        
        super.init()
    }
    
    @objc static func isConnectedAndRegistered() -> Bool {
        guard sharedInstance != nil else {
            return false
        }
        
        if (sharedInstance?.socket == nil || sharedInstance?.socket.isConnected() == false) {
            return false
        }
        if (sharedInstance?.socket.isRegistered() == false) {
            return false
        }
        return true
    }
    
    @objc func appDidEnterBackground(sender: Any) {
        //        SocketManager.disconnect();
    }
    
    @objc static func connectSocket() {
        if (SKApi.accessToken == nil) {
            return; // prevent to create socket if logout
        }
        SocketManager.getSharedInstance().connect()
    }
    
    private func connect() -> Void {
        socket.connect()
    }
    
    @objc static func disconnect() -> Void {
        guard let socketManager = sharedInstance else {
            // already disconnected
            return
        }
        //        NotificationCenter.default.removeObserver(socketManager);
        socketManager.socket.disconnect()
        socketManager.socket.unregisterAllObservers()
        
        socketManager.chatSocketObserver.unregisterAllListeners()
        sharedInstance = nil
    }
    
    // MARK: - API
    // just for objc
    @objc func sendApiRequest(stringMethod: String, api: String, data: NSDictionary!, callback: Socket.CallBackSocketRequest) {
        let requestMethod = Socket.convertStringRequestMethod(stringMethod: stringMethod)
        self.sendApiRequest(method: requestMethod, api: api, data: data, timeout: Socket.EmitTimeout, callback: callback)
    }
    
    @objc func sendApiRequest(stringMethod: String, api: String, data: NSDictionary!, timeout: Int = Socket.EmitTimeout, callback: Socket.CallBackSocketRequest) {
        let requestMethod = Socket.convertStringRequestMethod(stringMethod: stringMethod)
        self.sendApiRequest(method: requestMethod, api: api, data: data, timeout: timeout, callback: callback)
    }
    
    func sendApiRequest(method: Socket.RequestMethod, api: String, data: NSDictionary!, timeout: Int = Socket.EmitTimeout, callback: Socket.CallBackSocketRequest) {
        let request = Socket.createRequest(method: method, url: api, data: data)
        socket.sendApiRequest(request: request, timeout: timeout, callback: callback)
    }
    
    @objc func sendApiRequest(urlRequest: NSMutableURLRequest, callback: Socket.CallBackSocketRequest) {
        let request = Socket.createRequest(urlRequest: urlRequest)
        socket.sendApiRequest(request: request, callback: callback)
    }
    
    @objc func sendApiRequest(urlRequest: NSMutableURLRequest, timeout: Int, callback: Socket.CallBackSocketRequest) {
        let request = Socket.createRequest(urlRequest: urlRequest)
        socket.sendApiRequest(request: request, timeout: timeout, callback: callback)
    }
}
